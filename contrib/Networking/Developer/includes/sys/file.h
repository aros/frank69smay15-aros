#ifndef SYS_FILE_H
#define SYS_FILE_H
/*
    Copyright � 2003-2004, The AROS Development Team. All rights reserved.
    $Id$
*/

#ifndef UNISTD_H
#include <unistd.h>
#endif

#include <fcntl.h>

#endif /* !SYS_FILE_H */

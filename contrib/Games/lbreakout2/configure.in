dnl Process this file with autoconf to produce a configure script.
AC_INIT(Makefile.am)

AM_CONFIG_HEADER(config.h)

AM_INIT_AUTOMAKE(lbreakout2,2.4.1)

dnl Checks for programs.
AC_PROG_CC
AC_PROG_RANLIB
AC_EXEEXT

dnl Checks for libraries.
AC_CHECK_LIB(m, main,, AC_MSG_ERROR(lib math is needed))
AC_CHECK_LIB(z, main,, AC_MSG_ERROR(libz is needed))
AC_CHECK_LIB(png, main,, AC_MSG_ERROR(libpng is needed))

CFLAGS="$CFLAGS -Wall"

dnl check SDL version
AM_PATH_SDL(1.2.0,, AC_MSG_ERROR(lib SDL is needed))
CFLAGS="$CFLAGS $SDL_CFLAGS"
LIBS="$LIBS $SDL_LIBS"

dnl check warp mouse flag
warp_flag=""
AC_ARG_ENABLE( warp,
[  --enable-warp               Enable mouse warp (for BeOS and Mac)], warp_flag="-DMOUSE_WARP")
AC_SUBST(warp_flag)

dnl check sound
audio_flag="-DAUDIO_ENABLED"
mixer_flag="-lSDL_mixer"
AC_ARG_ENABLE( audio,
[  --disable-audio             Disables Audio], audio_flag=""; mixer_flag="")

dnl check if SDL_mixer's installed
dnl if not: clear sound_flag and mixer_flag
AC_CHECK_LIB(SDL_mixer, main,
AC_MSG_RESULT("SDL_Mixer found"), 
AC_MSG_RESULT("SDL_Mixer NOT found: Audio disabled"); audio_flag=""; mixer_flag="")

AC_SUBST(audio_flag)
AC_SUBST(mixer_flag)

dnl get host info
AC_CANONICAL_HOST

dnl documentation path
doc_dir="/usr/doc"
dnl check if manually overwriting doc path
AC_ARG_WITH( doc-path,
[  --with-doc-path=DPATH       Directory where documentation is saved.],
doc_dir="$withval",)
dnl substitute
AC_SUBST(doc_dir)

dnl network support
network_flag="-DNETWORK_ENABLED"
AC_ARG_ENABLE( network,
[  --disable-network           No network support.], network_flag="" )
AC_SUBST(network_flag)

dnl installation&highscore path
inst_dir="$datadir/games/lbreakout2"
hi_dir="/var/lib/games"

dnl check if installation was disabled
AC_ARG_ENABLE( install,
[  --disable-install           No installation. Played from the source directory.], 
inst_dir="." hi_dir="." )

dnl check if manually overwriting highscore path
AC_ARG_WITH( highscore-path,
[  --with-highscore-path=HPATH  Directory where global highscore chart is saved.],
hi_dir="$withval",)

dnl check if manually overwriting installation path
AC_ARG_WITH( inst-path,
[  --with-inst-path=IPATH  Directory where the game's data is places.],
inst_dir="$withval",)

inst_flag="-DSRC_DIR=\\\"$inst_dir\\\""
hi_inst_flag="-DHI_DIR=\\\"$hi_dir\\\""

AC_SUBST(inst_flag)
AC_SUBST(hi_inst_flag)
AC_SUBST(inst_dir)
AC_SUBST(hi_dir)

case "$host" in
	*-mingw32)
		win32="yes"
		win32_inst_script="`pwd -W`/installer.iss"
		arch_flag="-march=i586"
		win32_deps="lbreakout2res.o" ;;
	*) 
		win32="no"
		win32_inst_script=""
		arch_flag=""
		win32_deps="" ;;	
esac

AC_SUBST(win32_deps)
AC_SUBST(arch_flag)

if test "x$win32" = xyes; then 
	AC_PROG_AWK
	AC_PROG_STRIP
	AC_PROG_ISCC
	AC_SUBST(win32_inst_script)
fi

appname="LBreakout2"
AC_SUBST(appname)

AC_OUTPUT(
Makefile 
docs/Makefile 
common/Makefile 
gui/Makefile 
client/Makefile 
client/gui_theme/Makefile
client/gfx/Makefile 
client/gfx/AbsoluteB/Makefile 
client/gfx/Oz/Makefile 
client/gfx/Moiree/Makefile 
client/gfx/Classic/Makefile 
client/sounds/Makefile 
client/levels/Makefile 
server/Makefile)

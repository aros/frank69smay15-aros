
				LBreakout2
			    
		a breakout-style arcade game for Linux
			    by Michael Speck

********************
I.   Requirements
II.  Installation
III. Network Server
IV.  Documentation
V.   Troubleshooting
VI.  Resources
VII. Feedback
********************

********************
I.   Requirements
********************
LBreakout runs in X and uses SDL (any version though >= 1.1.4
is recommended) and SDL_mixer (optional for sound). Both libraries
can be found at http://libsdl.org. The graphical stuff requires PNG
which is usually included to a distro. If not check out
http://www.libpng.org/pub/png/libpng.html.

********************
II.  Installation
********************
Enter your favorite shell and type in the following stuff (in the
directory you extracted the LBreakout source)
> ./configure [--disable-audio] [--disable-install] [--enable-warp]
              [--with-highscore-path=HPATH] [--with-doc-path=DPATH]
              [--disable-network]
    --disable-audio:       no sound
    --disable-install:     no installation; play from source directory
    --enable-warp:         nescessary for non-Unices to use relative
                           mouse motion for inverting or modify mouse
                           speed
    --with-highscore-path: write global highscore chart lbreakout.hscr
                           to the specified directory
                           DEFAULT: /var/lib/games
    --with-doc-path:       install documentation to $DPATH/lbreakout2
                           DEFAULT: /usr/doc
    --disable-network:     compile LBreakout2 without any network 
                           support
> make
> su (become root)
> make install (if install wasn't disabled)
> exit (become user again)
> lbreakout2 (run client/non-network game)

!!! NOTE: Since 2.1.2 the config directory is ~/.lgames instead 
of ~/.lbreakout2. If you created levelsets with a previous version
you'll have to copy these sets from ~/.lbreakout2/levels to
~/.lgames/lbreakout2-levels !!!
The directory ~/.lbreakout2 is obsolete and may be deleted.

Building on Win32
-----------------
To build on a Win32 platform I used MinGW and MSYS.  
Network support is not available for the win32 version of lbreakout2.

1) Install the following win32 tools:
    MinGW
    MSYS
    Inno Setup 2
2) Install the following win32 libaries:
    SDL (you might need to edit sdl-config)
    SDL_mixer
    lipng
    zlib
3) From the build root, create win32dll directory and copy the following
   win32 dlls to it:
    SDL.dll
    SDL_mixer.dll
    libpng.dll
    zlib.dll
    msvcrt.dll (freely available from Microsoft)
4) configure will need to find SDL.dll. You can either make sure SDL.dll
   is in your PATH or simply copy it to the root build directory.
5) Setup environment.  To get things to compile, I setup the following
   environment variables:
    SDL_CONFIG  - fully qualified path to sdl-config
    CFLAGS      - tell build process where to find include files
    LDFLAGS     - tell build process where to find libraries
    PATH        - ensure that Inno Setup 2 iscc and zip are in the PATH
6) Do a full build by issueing the following commands:
    configure --disable-install --disable-network
    make
7) Create a win32 installer by issueing the following command:
    make win32-installer
 
You'll find lbreakout2-<version>-win32.exe in the build director. This is
the is a standalone installer for lbreakout2.

********************
III. Network Server
********************
To run a server start lbreakout2server with any of the following
options:
-p <PORT>     server port (default 2002)
-l <LIMIT>    maximum number of users that may connect to the server 
              (default is 30)
-i <IDLETIME> a user will be kicked if he didn't communicate with the 
              server for this amount of seconds (default is 1200)
-n <MESSAGE>  this message is displayed to welcome a user
-a <PASSWORD> a user that logs in with this name will become 
              administrator named admin (default is no admin)
After that you'll have to query your IP (e.g. with 
/sbin/ifconfig) and pass it to your friends as there is no permanent
internet server yet. Note, that you shouldn't use localhost as you
won't be able to challenge others then. 
Check the online documentation at http://lgames.sf.net 
for information on how to play.

********************
IV.  Documentation
********************
If you have any questions about options, game play or editor
please check the documentation and see if you
can find an answer there. I will not respond to any eMails concerning
questions easily answered by the manual (/usr/doc/lbreakout2).
However, if you have any other problems or suggestions or you found a
bug please contact me: kulkanie@gmx.net

********************
V.   Troubleshooting
********************
The solution for relative mouse motion (nescessary when inverting or
slowing down mouse by motion modifier) works fine for Linux and 
Win32 but fails for other non-Unices. Current solution is to warp the 
mouse (by using configure option --enable-warp). Unfortunately, this 
will handicap events when trying to fire weapon or release balls AND 
move the paddle at the same time. This means it may happen that a weapon
is not fired or doesn't stop fire, balls keep being attached and so 
on...
---
Disabling sound while playing seems to result in loosing various
sound channels when some sounds were actually mixed.
---
Someone reported that he had problems with PNG (configure script
didn't find it) if it was installed to /usr/local/lib. 
Setting a link in /usr/lib fixes this.
---
If you have SDL_mixer installed but configure tells you that it can't 
find it, remember that you have to install the development package
for compiling!
---
If you can't compile because LBreakout2' timestamps are slightly in 
the future run 'touchall' (found in the configure directory).
---
If 'fullscreen' just adds a black frame around the game but does
not change the solution make sure that you have 640x480 available
as resolution in your Xconfig.
---
If the SDL sound seems to be out of sync first try
to modify the audio buffer size in ~/.lgames/lbreakout2.conf.
If this fails set SDL_AUDIODRIVER to dma (export SDL_AUDIODRIVER=dma).
 
********************
VI.  Resources
********************
Some graphics and sounds has been taken and modified from other
non-copyrighted resources:
Backgrounds:    http://www.grsites.com/textures
Sounds:         "Web Clip Empire 50.000", NovaMedia Verlag, Germany
Thanks to all of these guys for there free stuff!

********************
VII. Feedback
********************
LGames URL:  http://www.lgames.org
e-Mail:      kulkanie@gmx.net


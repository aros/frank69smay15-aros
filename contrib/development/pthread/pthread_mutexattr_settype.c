/*
    Copyright � 2007, The AROS Development Team. All rights reserved.
    $Id$
*/

#include <aros/debug.h>

#include <pthread.h>

int pthread_mutexattr_settype(pthread_mutexattr_t* attr, int type)
{
#   warning Implement pthread_mutexattr_settype()
    AROS_FUNCTION_NOT_IMPLEMENTED("pthread_mutexattr_settype");
	
    return 0;
}

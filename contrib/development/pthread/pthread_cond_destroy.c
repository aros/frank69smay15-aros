/*
    Copyright � 2007, The AROS Development Team. All rights reserved.
    $Id$
*/

#include <aros/debug.h>

#include <pthread.h>

int pthread_cond_destroy(pthread_cond_t* cond)
{
#   warning Implement pthread_cond_destroy()
    AROS_FUNCTION_NOT_IMPLEMENTED("pthread_cond_destroy");
	
    return 0;
}

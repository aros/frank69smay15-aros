/*
    Copyright © 2011, The AROS Development Team. All rights reserved.
    $Id$
*/

#include <aros/debug.h>
#include <pthread.h>

int   pthread_key_create(pthread_key_t * k , void (*f)(void *))
{
#   warning Implement pthread_key_create()
    AROS_FUNCTION_NOT_IMPLEMENTED("pthread_key_create");
    return 0;
}

/*
    Copyright � 2007, The AROS Development Team. All rights reserved.
    $Id$
*/

#include <aros/debug.h>

#include <pthread.h>

int pthread_attr_destroy(pthread_attr_t* attr)
{
#   warning Implement pthread_attr_destroy()
    AROS_FUNCTION_NOT_IMPLEMENTED("pthread_attr_destroy");
	
    return 0;
}

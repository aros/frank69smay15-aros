/*
    Copyright © 2011, The AROS Development Team. All rights reserved.
    $Id$
*/

#include <aros/debug.h>
#include <pthread.h>

void pthread_exit(void * a)
{
#   warning Implement pthread_exit()
    AROS_FUNCTION_NOT_IMPLEMENTED("pthread_exit");
}

/*
    Copyright � 2007, The AROS Development Team. All rights reserved.
    $Id$
*/

#include <aros/debug.h>

#include <pthread.h>

int pthread_mutexattr_destroy(pthread_mutexattr_t* attr)
{
#   warning Implement pthread_mutexattr_destroy()
    AROS_FUNCTION_NOT_IMPLEMENTED("pthread_mutexattr_destroy");
	
    return 0;
}

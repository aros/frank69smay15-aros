/*
    Copyright � 2007, The AROS Development Team. All rights reserved.
    $Id$
*/

#include <aros/debug.h>

#include <pthread.h>

int pthread_cond_init(
    pthread_cond_t*           cond,
    const pthread_condattr_t* attr)
{
#   warning Implement pthread_cond_init()
    AROS_FUNCTION_NOT_IMPLEMENTED("pthread_cond_init");
	
    return 0;
}

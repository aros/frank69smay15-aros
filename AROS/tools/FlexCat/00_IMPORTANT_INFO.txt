
FORKS AND DEVELOPMENT
----------------------------
I no longer develop nor maintain FlexCat. Last version from me is v2.4
and all files are in this folder (or http://wfmh.org.pl/pub/flexcat).
But it looks FlexCat is not dead. There's actively developed fork on
Source Forge: http://sourceforge.net/projects/flexcat/ so I recommend
checking their code instead, especially it looks like app is now
ported to more platforms we formerly had and incorporates some
fixes as well.


LICENSE
----------------------------
Some people are wondering about license - FlexCat formerly was and
still is GPLed. Period. Archives shall contain GPL's readme 
but I did not add any. Still, take my word and respect the license :)
All about GPL: http://www.gnu.org/licenses/gpl.html


CHANGES AND PATCHES
----------------------------
Some developers pointed to the 'developer.readme' file within
source archive (FlexCat_Src.lha) asking how shall it be understood
as I put the request to not release forked FlexCat. Well, it was mainly
added to avoid many FlexCats clones with insignificiant differences
floating around (which at some point was a bit of "trend" in Amiga world). 
Hopefuly FlexCat was not so attractive to "developers" and nothing like
that happened. So it's mostly dead request. But anyway - it was not
a requirement but just a kind request. GPL gives you right to fork
no matter who asks for what. I am glad people bothers developer's will
sometimes and come asking about interpretation :) So here it is: 
if you want to fork - my recommendation is to grab SourceForge's fork
code to work on - it's much recent. If you want to fork my v2.4 - feel
free, but again - I'd suggest using SF's code. If you still here, made
any changes to the FlexCat and want to share you code with me - please
do not. I am not longer maintaining FlexCat which means I will not be
able to merge your changes nor release anything beyond v2.4. And knowing
about SF's fork I do not event want to. v2.4 is last version from me,
and as such should e considered final version and at the same time 
*frozen* code. No further patches nor changes are planned to be made
or incorporate. So again - I appreciate all the efforts and interest
in the FlexCat and I am glad it's still alive after all this years, but 
let me one more time point you to SF's fork as the one to patch/fork/use/reuse.

Regards,
Marcin Orlowski
carlos - wfmh - org - pl
Dated: 15 Jan 2011

#ifndef _AROS_TYPES_PTRDIFF_T_H
#define _AROS_TYPES_PTRDIFF_T_H

/*
    Copyright © 2010-2011, The AROS Development Team. All rights reserved.
    $Id: /aros/branches/ABI_V1/trunk-aroscsplit/AROS/compiler/arosstdc/include/aros/types/ptrdiff_t.h 35477 2010-11-07T22:33:25.524511Z verhaegs  $

    ptrdiff_t
*/

typedef long int ptrdiff_t;

#endif /* _AROS_TYPES_PTRDIFF_T_H */

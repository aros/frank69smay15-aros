#ifndef _AROS_TYPES_MBSTATE_T_H
#define _AROS_TYPES_MBSTATE_T_H

/*
    Copyright © 2010-2011, The AROS Development Team. All rights reserved.
    $Id: /aros/branches/ABI_V1/trunk-aroscsplit/AROS/compiler/arosstdc/include/aros/types/mbstate_t.h 36769 2011-01-11T21:35:35.950252Z verhaegs  $
*/

typedef int mbstate_t;

#endif /* _AROS_TYPES_MBSTATE_T_H */

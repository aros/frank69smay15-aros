#ifndef _AROS_TYPES_SEEK_H
#define _AROS_TYPES_SEEK_H

/*
    Copyright © 2010-2011, The AROS Development Team. All rights reserved.
    $Id: /aros/branches/ABI_V1/trunk-aroscsplit/AROS/compiler/arosstdc/include/aros/types/seek.h 35732 2010-11-14T17:46:11.161641Z verhaegs  $

    SEEK_SET, SEEK_CUR, SEEK_END
*/

#define SEEK_SET    0           /* standard input file descriptor */
#define SEEK_CUR    1           /* standard output file descriptor */
#define SEEK_END    2           /* standard error file descriptor */

#endif /* _AROS_TYPES_SEEK_H */

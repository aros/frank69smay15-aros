/*
    Copyright 2011, The AROS Development Team. All rights reserved.
    $Id$
*/

#ifndef GLU_INTERN_H
#define GLU_INTERN_H

#include <proto/exec.h>

/****************************************************************************************/

struct GLUBase
{
    struct Library  mglb_Lib;
};

#endif /* GLU_INTERN_H */

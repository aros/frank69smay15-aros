/*
    Copyright 2011, The AROS Development Team. All rights reserved.
    $Id$
*/

#ifndef EGL_INTERN_H
#define EGL_INTERN_H

#include <proto/exec.h>

/****************************************************************************************/

struct EGLBase
{
    struct Library  mglb_Lib;
};

#endif /* EGL_INTERN_H */

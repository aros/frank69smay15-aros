/*
    Copyright 2011, The AROS Development Team. All rights reserved.
    $Id$
*/

#ifndef VEGA_INTERN_H
#define VEGA_INTERN_H

#include <proto/exec.h>

/****************************************************************************************/

struct VegaBase
{
    struct Library  mglb_Lib;
};

#endif /* VEGA_INTERN_H */

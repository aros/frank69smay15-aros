/*
    Copyright 2009, The AROS Development Team. All rights reserved.
    $Id$
*/

#ifndef AROSMESA_INTERN_H
#define AROSMESA_INTERN_H

#include <proto/exec.h>

/****************************************************************************************/

struct MesaBase
{
    struct Library  mglb_Lib;
};

#endif /* AROSMESA_INTERN_H */

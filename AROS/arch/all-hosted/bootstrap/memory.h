void *AllocateRO(size_t len);
void *AllocateRW(size_t len);
void *AllocateRAM(size_t len);
int SetRO(void *addr, size_t len);

void *krnAllocBootMem(unsigned int size);
void *krnAllocBootMemAligned(unsigned int size, unsigned int align);
